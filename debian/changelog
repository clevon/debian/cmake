cmake (3.30.0~rc3-1) unstable; urgency=medium

  * Update upstream.

 -- Mairi Dubik <mairi.dubik@clevon.com>  Sun, 16 Jun 2024 13:34:20 +0300

cmake (3.29.0~rc3-1) unstable; urgency=medium

  * New upstream release.
  * Don't append Git revision to version since it'll be for the wrong branch.
  * Try .cmake files when trying system toolchains.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 11 Mar 2024 19:54:14 +0200

cmake (3.28.3-1) unstable; urgency=medium

  * New upstream release.
  * Change default generator to Ninja.
  * Support loading toolchain files from a system directory.

 -- Raul Tambre <raul.tambre@clevon.com>  Thu, 08 Feb 2024 17:10:43 +0200

cmake (3.28.1-1) unstable; urgency=medium

  * New upstream release.
  * Refresh copyright information.
  * Run tests in parallel according to DEB_BUILD_OPTIONS's parallel option.

 -- Raul Tambre <raul.tambre@clevon.com>  Wed, 20 Dec 2023 14:59:30 +0200

cmake (3.23.0-1) unstable; urgency=medium

  * New upstream release (Closes: #1).
  * Support nodoc and nocheck profiles.
  * Install documentation into main package's folder.

 -- Raul Tambre <raul.tambre@clevon.com>  Mon, 04 Apr 2022 11:55:50 +0300

cmake (3.20.3-2) unstable; urgency=medium

  * Move vim syntax to cmake-data package.
  * Mark cmake-data as Multi-Arch: foreign.
  * Drop a few unused Build-Depends.
  * Simplify documentation installation.
  * Improve package descriptions.

 -- Raul Tambre <raul@tambre.ee>  Wed, 09 Jun 2021 13:08:07 +0300

cmake (3.20.3-1) unstable; urgency=medium

  * Update upstream.
  * Modernize packaging.

 -- Raul Tambre <raul@tambre.ee>  Tue, 08 Jun 2021 14:57:11 +0300

cmake (3.19.0-1~cleveron1) unstable; urgency=medium

  * Update upstream.
  * Bump standards versions.
  * Remove manual compile flags, that should be up to the packager.
  * Simplify build scripts greatly by not bootstrapping.
  * Use LTO.

 -- Raul Tambre <raul.tambre@cleveron.com>  Thu, 16 Jul 2020 11:06:03 +0300

cmake (3.18.0-1~cleveron2) unstable; urgency=medium

  * Update with CUDA architecture fix.

 -- Raul Tambre <raul.tambre@cleveron.com>  Mon, 05 Jun 2020 08:03:52 +0200

cmake (3.18.0-1~cleveron1) unstable; urgency=medium

  * Update upstream.
  * Remove "-march=native" from default flags.

 -- Raul Tambre <raul.tambre@cleveron.com>  Mon, 05 Jun 2020 08:03:52 +0200

cmake (3.17.0-1~cleveron9) unstable; urgency=medium

  * Update upstream.

 -- Raul Tambre <raul.tambre@cleveron.com>  Mon, 06 Apr 2020 19:11:30 +0200

cmake (3.17.0-1~cleveron8) unstable; urgency=medium

  * Update upstream.
  * Remove FindPkgConfig change.

 -- Raul Tambre <raul.tambre@cleveron.com>  Sun, 18 Mar 2020 21:12:15 +0200

cmake (3.17.0-1~cleveron7) unstable; urgency=medium

  * Update unmerged changes for Clang CUDA standard flags support.

 -- Raul Tambre <raul.tambre@cleveron.com>  Sun, 16 Mar 2020 11:53:27 +0200

cmake (3.17.0-1~cleveron6) unstable; urgency=medium

  * Fix unmerged changes not being present.

 -- Raul Tambre <raul.tambre@cleveron.com>  Sun, 15 Mar 2020 19:32:08 +0200

cmake (3.17.0-1~cleveron5) unstable; urgency=medium

  * Upstream updated.
  * Includes following unmerged changes:
    * Clang CUDA support (!4442).
    * PkgConfig crosscompile support (!4478).

 -- Raul Tambre <raul.tambre@cleveron.com>  Sun, 15 Mar 2020 18:46:01 +0200

cmake (3.17.0-1~cleveron4) unstable; urgency=medium

  * Upstream updated.
  * Remove upstreamed JSON compile fix.

 -- Raul Tambre <raul.tambre@cleveron.com>  Wed, 05 Feb 2020 09:27:01 +0200

cmake (3.17.0-1~cleveron3) unstable; urgency=medium

  * Upstream updated.
  * Compile with -march=native -O3, disable some hardening options.
  * Patch to fix a JSON compile error.

 -- Raul Tambre <raul.tambre@cleveron.com>  Tue, 28 Jan 2020 07:56:53 +0200

cmake (3.17.0-1~cleveron2) unstable; urgency=medium

  * CUDA nvcc 10.2.19 version checks fixed.

 -- Raul Tambre <raul.tambre@cleveron.com>  Tue, 24 Nov 2019 12:13:03 +0200

cmake (3.17.0-1~cleveron1) unstable; urgency=medium

  * New upstream code.

 -- Raul Tambre <raul.tambre@cleveron.com>  Mon, 23 Nov 2019 16:52:52 +0200
